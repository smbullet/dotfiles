set nocompatible " required for Vundle
filetype off     " required for Vundle

" set Vundle runtime path and initialize it
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Bundle
Plugin 'VundleVim/Vundle.vim'
Plugin 'cakebaker/scss-syntax.vim'
Plugin 'Raimondi/YAIFA'

call vundle#end()

filetype indent plugin on
syntax on

set number
set relativenumber

colorscheme molokai

imap jk <Esc>

set backspace=indent,eol,start
