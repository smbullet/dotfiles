# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/eric/.zshrc'

fpath=(~/.zsh/completion $fpath)

autoload -Uz compinit
compinit
# End of lines added by compinstall

autoload -U colors && colors

source ~/.git-prompt.sh
GIT_PS1_SHOWDIRTYSTATE=true
setopt PROMPT_SUBST

PROMPT_HOSTNAME="%{%B%F{white}%}[%{%F{green}%}%M%{%F{white}%}]"
PROMPT_DIR="%{%F{white}%}[%{%F{cyan}%}%1~%{%F{white}%}]"
# This MUST be in single quotes. Otherwise it gets evaluated once upon being
# sourced and doesn't update when changing directories.
PROMPT='$PROMPT_HOSTNAME $PROMPT_DIR %F{red}$(__git_ps1 "(%s) ")%{$reset_color%}$ '

KEYTIMEOUT=10
bindkey jk vi-cmd-mode

bindkey "^P" up-line-or-search
