SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
ln -s $SCRIPTPATH/.gitconfig ~/ --force
ln -s $SCRIPTPATH/.git-prompt.sh ~/ --force
ln -s $SCRIPTPATH/.tmux.conf ~/ --force
ln -s $SCRIPTPATH/.vim ~/ --force
ln -s $SCRIPTPATH/.vimrc ~/ --force
ln -s $SCRIPTPATH/.zprofile ~/ --force
ln -s $SCRIPTPATH/.zshrc ~/ --force

git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
